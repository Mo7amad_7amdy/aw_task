<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/my-books/{id}', 'myBookController@show')->name('myBook');
    Route::get('/book/{id}', 'BookController@show')->name('book');
    Route::post('/book/save', 'BookController@store')->name('saveBook');
    Route::get('/book/delete/{id}', 'BookController@destroy')->name('deleteBook');
});
