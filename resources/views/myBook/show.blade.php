@extends('layouts.app')
@section('title') My Books @endsection
@section('content')
    <div class="container" id="app">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Here is My Books</div>
                    <div class="card-body">
                        @forelse($books as $book)
                            <div class="col-md-12 book-box">
                                <a href="{{ route('deleteBook', $book->id) }}" class="delete-book"><i class="fas fa-trash-alt"></i></a>
                                <img src="{{ url($book->image) }}" title="Book Name" alt="Book Name" width="140"
                                     height="220">
                                <div class="book-box-content">
                                    <h1>{{$book->title}}</h1>
                                    <label>Author: </label>
                                    <h3>{{ $book->author }}</h3>

                                    <label>Publisher: </label>
                                    <h5> {{ $book->publisher }}</h5>

                                    <a href="{{ route('book', $book->book_id) }}" class="btn btn-dark col-md-3"
                                       target="_blank">Preview Book</a>

                                </div>
                            </div>
                        @empty
                            <div class='text-center'>
                                <div class='alert alert-info' role='alert'><strong>Heads up!</strong> No Data to Show!</div>
                            </div>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
