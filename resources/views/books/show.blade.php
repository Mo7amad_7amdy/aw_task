@extends('layouts.app')

@section('title') {{ $book->volumeInfo->title }} @endsection
@section('content')

    <div class="container" id="app">
        <div class="row justify-content-center">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ $book->volumeInfo->title }}</div>
                    <div class="card-body">

                        <div id="messages" class="col-md-12"></div>
                        <form>

                            <div class="col-md-12 book-box-2">
                                <div class="col-md-12">
                                    @if(empty($book->volumeInfo->imageLinks->thumbnail))

                                        <img src="{{ asset('image/book.jpg') }}" title="Book Name"
                                             alt="Book Name"
                                             width="140" height="220">

                                    @else

                                        <img src="{{ url($book->volumeInfo->imageLinks->thumbnail) }}" title="Book Name"
                                             alt="Book Name"
                                             width="140" height="220">

                                    @endif
                                    <div class="book-box-content">

                                        <h1 id="title">{{ $book->volumeInfo->title }}</h1>

                                        <label>Author: </label>
                                        <h3 id="author">@foreach($book->volumeInfo->authors as $author) {{ $author }}
                                            , @endforeach</h3>

                                        <label>Publisher: </label>
                                        <h5 id="publisher">{{ $book->volumeInfo->publisher }}</h5>

                                        <label>Published date: </label>
                                        <h5>{{ $book->volumeInfo->publishedDate }}</h5>

                                        <h5><label>Page Count: </label> {{ $book->volumeInfo->pageCount }}</h5>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label>Description: </label>
                                    <p>{!! empty($book->volumeInfo->description) ? 'no description to show' : $book->volumeInfo->description !!}</p>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-info col-md-3" id="addBook">Add To My Books</button>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')

    <script>
        jQuery(document).ready(function () {
            jQuery('#addBook').click(function (e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                axios({
                    method: 'post',
                    url: "{{ route('saveBook') }}",
                    data: {
                        title: "{{ $book->volumeInfo->title }}",
                        user_id: "{{ Auth::user()->id }}",
                        book_id: "{{ $book->id }}",
                        author: "@foreach($book->volumeInfo->authors as $author) {{ $author }}, @endforeach",
                        image: "{{ url($book->volumeInfo->imageLinks->thumbnail) }}",
                        publisher: "{{ $book->volumeInfo->publisher }}"
                    },

                })
                    .then(function (data) {
                        messages.innerHTML = "<div class='text-center'><div class='alert alert-success' role='alert'><strong>Well done!</strong> You successfully add this book to you books list!.</div></div>"
                        console.log(data)
                    })
                    .catch(function (error) {
                        console.log(error);
                    })
            });
        });
    </script>

@endsection
