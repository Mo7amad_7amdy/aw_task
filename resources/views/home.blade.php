@extends('layouts.app')

@section('title') Search for books by name of book, Author, category.. @endsection
@section('content')

    <div class="container" id="app">
        <div class="row justify-content-center">

            <div class="col-md-12">
                <div class="input-group">
                    <input id="search" class="form-control" placeholder="Search for books by name of book, Author, category..">
                    <span class="input-group-btn">
                        <button id="button" class="btn btn-default" @click="getBooks()">Go!</button>
                    </span>
                </div><!-- /input-group -->
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Result</div>
                    <div class="card-body">
                        <div class="row">

                            <div id="loading" class="col-md-12 text-center" style='display: none;'>
                                <img src="{{ asset('image/ajax-loader.gif') }}" alt="loading" title="loading">
                            </div>

                            <div id="messages" class="col-md-12"></div>

                            <div class="col-md-12 book-box" v-for="book in results">
                                <img :src="book.volumeInfo.imageLinks.thumbnail" title="Book Name" alt="Book Name"
                                     width="140" height="220">
                                <div class="book-box-content">
                                    <h1>@{{book.volumeInfo.title}}</h1>
                                    <label>Author: </label>
                                    <h3 v-for="author in book.volumeInfo.authors">@{{ author }}</h3>

                                    <label>Publisher & Category: </label>
                                    <h5> @{{ book.volumeInfo.publisher }}</h5><h5 v-for="category in book.volumeInfo.categories">@{{ category }}</h5>

                                    @auth <a :href="'book/'+book.id" class="btn btn-dark col-md-3" target="_blank">Preview Book</a> @endauth

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script>

        var app = new Vue({
            el: '#app',
            data: {
                results: []
            },
            methods: {
                getBooks() {
                    var search = document.getElementById('search').value;
                    document.getElementById('messages').innerHTML = "";
                    console.log(search);
                    if (search === '') {
                        messages.innerHTML = "<div class='text-center'><div class='alert alert-warning' role='alert'><strong>Warning!</strong> Please write something to search!</div></div>"
                    } else {
                        $.ajax({
                            url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
                            dataType: "json",
                            beforeSend: function () {
                                $("#loading").show();
                            },
                            success: (data) => this.fetchBooks(data),
                            complete: function (data) {
                                $("#loading").hide();
                            },
                            type: 'GET',
                        });
                    }
                    console.log(this.results)

                },
                fetchBooks(data) {
                    if(!data.items){
                        messages.innerHTML = "<div class='text-center'><div class='alert alert-info' role='alert'><strong>Heads up!</strong> No Data to Show!</div></div>"
                    }
                    else {
                        this.results = data.items;

                    }
                }
            }
        });
    </script>
@endsection
