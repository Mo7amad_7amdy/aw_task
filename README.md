## Google Books API

this is the technical task for AWStreams created by Mohamed Hamdy, this project helps users to search for books in google books and preview them and add them to thire books for further use.

## Technologies used

- Laravel Framework.
- Vue.js.
- Ajax.
- Axios.
- Bootstrap


######Installation:
you should write this command in console 
```
php artisan migrate
```
to migrate table in database. 

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=aw_task
DB_USERNAME=root
DB_PASSWORD=''
```

###how it was developed: 

- searching and displaying books: used vue.js to create URL request and parsing result and display data.
- add to my Books:  used Axios to get the selected book data and save it.
