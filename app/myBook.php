<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class myBook extends Model
{
    protected $table    = 'my_books';
    protected $fillable = [
        'user_id',
        'book_id',
        'title',
        'author',
        'publisher',
        'image',
    ];
    public function user(){
        return $this->belongsTo('App\User', 'book_id');
    }
}
